"""
Created on Dec 11, 2014

Copyright (C) 2014 and onwards  Weiliang Zhang, All rights reserved.

Commercial use is prohibited without permission from Weiliang Zhang.

@author: Weiliang Zhang
@email: weiliang.zhang@gmail.com
"""

# TODO FINISH SRCH query

import blpapi as blp
import numpy as np
import pandas as pd
import datetime as dt
import itertools as itr
import pprint

# import logging as log
from typing import Tuple, Dict, Union, Iterable

import sys
import xarray as xr
import tqdm

# from pyblp.symbols import *
# from .symbols.symbols import *
# from pyblp import symbols as S
from . import symbols as S


REF_DATA = "//blp/refdata"
SRCH_SERVICE = "//blp/exrsvc"

HIST_REQUEST = "HistoricalDataRequest"
REF_REQUEST = "ReferenceDataRequest"

PERIODICITY = "periodicitySelection"
INTERVAL_SUM = "INTERVAL_SUM"

BBG_DATE_FORMAT = "%Y%m%d"

# logger = log.Logger()
# todo: set up logger


def dict_to_xarray(dict_df, index=None, columns=None):
    """
    Convert dict of dataframes to xarray DataArray.

    Parameters:
    dict_df:
        dict of dataframes
    index:
        string name of the dictionary's keys, used as a coordinate
    columns:
        string names of the index and columns of the dataframes, should be
        2 names, both used as coordinates.
    """
    if index is None:
        index = "keys"
    if columns is None:
        columns = ["index", "columns"]

    variables = {
        k: (
            xr.DataArray(v, dims=columns)
            if isinstance(v, pd.DataFrame)
            else xr.DataArray(v, dims=["index"])
        )
        for k, v in dict_df.items()
    }
    # combined = xr.Dataset(variables).to_array(dim=index)
    combined = xr.Dataset(variables)
    return combined


def is_service_down(event):
    if event.eventType() == blp.Event.SERVICE_STATUS:
        for msg in event:
            if msg.messageType() == "ServiceDown":
                print(msg)
                return True
        return False
    else:
        return False


def generic_fetcher(
    service, request_builder, msg_handler, request_args, msg_args
):
    session = blp.Session()
    if not session.start():
        print("Cannot start Bloomberg session.")
        return
    if not session.openService(service):
        print("Cannot open service = {}".format(service))
        return

    request = request_builder(session, request_args)

    session.sendRequest(request)

    result = msg_handler(session)

    print("Completed, session shutdown = {}".format(session.stop()))

    return result


def msg_stdio(session):
    while True:
        ev = session.nextEvent()
        if (
            ev.eventType() == blp.Event.RESPONSE
            or ev.eventType() == blp.Event.PARTIAL_RESPONSE
        ):
            for msg in ev:
                print(msg)
            if ev.eventType() == blp.Event.RESPONSE:
                break
        elif (
            ev.eventType() == blp.Event.UNKNOWN
            or ev.eventType() == blp.Event.TIMEOUT
        ):
            for msg in ev:
                print(msg)
                break
        # ignore the service and session events
    return None


# TODO RE-WRITE EVENT PROCESSING WITH DECORATORS


def srch_request_builder(session, srch_name):
    service = session.getService(SRCH_SERVICE)
    request = service.createRequest("ExcelGetGridRequest")
    domain = request.getElement("Domain")
    domain.setValue("fi:" + srch_name)
    return request


class DefaultEventHandler:
    """
    Parent, detault event handler which prints to stdio
    """

    def handle(self, session, **kwargs):
        verbose = kwargs.get("verbose", False)

        while True:
            ev = session.nextEvent()
            if (
                ev.eventType() == blp.Event.RESPONSE
                or ev.eventType() == blp.Event.PARTIAL_RESPONSE
            ):
                for msg in ev:
                    print(msg)
                if ev.eventType() == blp.Event.RESPONSE:
                    break
            elif (
                blp.Event.TIMEOUT == ev.eventType()
                or blp.Event.UNKNOWN == ev.eventType()
            ):
                for msg in ev:
                    print(msg)
                raise AssertionError("Timed out or UNKNOWN Event")
            elif is_service_down(ev):
                raise AssertionError("Bloomberg Service Down")
            elif verbose:
                for msg in ev:
                    print(msg)


class SRCHHandler(DefaultEventHandler):
    """
    SRCH event handler
    """

    def handle(self, session, **kwargs):
        results = set()
        alive = True
        while alive:
            #
            ev = session.nextEvent()
            if (
                ev.eventType() == blp.Event.RESPONSE
                or ev.eventType() == blp.Event.PARTIAL_RESPONSE
            ):
                for msg in ev:
                    if not msg.hasElement("DataRecords"):
                        print("Cant" "t find DataRecords[]")
                        print(msg)
                        continue
                    dataRecords = msg.getElement("DataRecords")
                    if dataRecords.isArray():
                        for record in dataRecords.values():
                            fields = record.getElement("DataFields")
                            if fields.isArray():
                                for field in fields.values():
                                    if field.hasElement("StringValue"):
                                        elm = field.getElement("StringValue")
                                        results.add(elm.getValue())
                                    else:
                                        print("Strange DataField=")
                                        print(field)
                            else:
                                print("Strange DataFields[]=")
                                print(fields)
                    else:
                        print("Non-Array DataRecords=")
                        print(dataRecords)
                # msg processed
                if ev.eventType() == blp.Event.RESPONSE:
                    alive = False
                    break
            elif (
                blp.Event.TIMEOUT == ev.eventType()
                or blp.Event.UNKNOWN == ev.eventType()
            ):
                print("TIMEOUT or UNKNOWN Event...")
                for msg in ev:
                    print(msg)
            elif is_service_down(ev):
                raise AssertionError("Bloomberg Service Down")
        return results


def get_srch_bonds(srch_name, verbose=False) -> Iterable[str]:
    """
    Get bond identifier from a saved SRCH.

    Returns: bloomberg identifiers in a list. Chopping off the security
    type strings.
    """
    data = run_srch(srch_name, verbose)
    bonds = [x.split(" ")[0] for x in data]
    return bonds


def run_srch(srch_name, verbose=False):
    return run_query(
        service_name="//blp/exrsvc",
        request_builder=srch_request_builder,
        request_args=srch_name,
        event_handler=SRCHHandler(),
        verbose=verbose,
    )


def run_query(
    service_name,
    request_builder,
    request_args=None,
    event_handler=DefaultEventHandler(),
    verbose=False,
    **handler_args,
):
    session = blp.Session()
    if not session.start():
        raise AssertionError("Cannot start Bloomberg Session")
    if not session.openService(service_name):
        raise AssertionError("Cannot open service = {}".format(service_name))

    request = request_builder(session, request_args)

    if verbose:
        print("Sending Request...")
    session.sendRequest(request)

    try:
        data = event_handler.handle(session, **handler_args)
    except AssertionError:
        session.stop()
        raise AssertionError("No valid result.")

    print("Query Completed, session closed.")
    return data


def cashflow_mapper(array_element):
    """
    Use with getRefBulk to extract cash flow data
    returns cashflow in dict.
    """
    cashflow = dict()
    # BB msg tags
    PAYMENT_DATE = "Payment Date"
    PRINCIPAL = "Principal Amount"
    COUPON = "Coupon Amount"

    for element in array_element.values():
        # get principal
        if element.hasElement(PAYMENT_DATE):
            field = element.getElement(PAYMENT_DATE)
            pay_date = field.getValue()
        else:
            # no payment date, print and ignore
            print("Unusual format: No Payment Date.")
            print(element)

        if element.hasElement(PRINCIPAL):
            field = element.getElement(PRINCIPAL)
            principal = field.getValue()
            if principal != 0:
                cashflow[pay_date] = cashflow.get(pay_date, 0) + principal
        else:
            print("Unusual format: No Principal Amount.")
            print(element)

        if element.hasElement(COUPON):
            field = element.getElement(COUPON)
            coupon = field.getValue()
            if coupon != 0:
                cashflow[pay_date] = cashflow.get(pay_date, 0) + coupon
        else:
            print("Unusual format: No Coupon Amount.")
            print(element)

    return cashflow


def get_cashflow(secs, debug=False):
    """
    Get cashflow for given securities.
    """
    data = getRefBulk(
        securities=secs,
        fields=S.DES_CASH_FLOW,
        overrides=None,
        array_mapper=cashflow_mapper,
        debug=debug,
    )
    # return pd.Panel.from_dict(data=data).minor_xs(DES_CASH_FLOW)
    ds = dict_to_xarray(data)
    df = ds.sel(fields=S.DES_CASH_FLOW).to_dataframe()
    # drop row lable column
    df.drop("fields", axis=1, inplace=True)
    return df


# def _nested_dict_to_xarray_optimized(data_dict) -> xr.DataArray:
#     """
#     Convert nested dictionary {ticker: {datetime: {field: value}}} to xarray DataArray.
#     Optimized version using numpy operations and minimal transformations.

#     Generated by Claude 3.5 Sonnet on 2025-01-05.
#     """
#     # First pass: collect unique values using sets for faster lookups
#     dates = set()
#     fields = set()
#     for ticker_data in data_dict.values():
#         for date, field_data in ticker_data.items():
#             dates.add(pd.to_datetime(date))
#             fields.update(field_data.keys())

#     # Convert to sorted lists for indexing
#     sorted_dates = sorted(dates)
#     sorted_tickers = sorted(data_dict.keys())
#     sorted_fields = sorted(fields)

#     # Create mapping dictionaries for quick index lookups
#     date_to_idx = {date: idx for idx, date in enumerate(sorted_dates)}
#     ticker_to_idx = {ticker: idx for idx, ticker in enumerate(sorted_tickers)}
#     field_to_idx = {field: idx for idx, field in enumerate(sorted_fields)}

#     # Pre-allocate numpy array
#     shape = (len(dates), len(sorted_tickers), len(sorted_fields))
#     values = np.full(shape, np.nan)

#     # Fill values using direct numpy indexing
#     for ticker, ticker_data in data_dict.items():
#         ticker_idx = ticker_to_idx[ticker]
#         for date_str, field_data in ticker_data.items():
#             date_idx = date_to_idx[pd.to_datetime(date_str)]
#             for field, value in field_data.items():
#                 field_idx = field_to_idx[field]
#                 values[date_idx, ticker_idx, field_idx] = value

#     # Create DataArray in one shot
#     return xr.DataArray(
#         values,
#         coords={
#             "datetime": sorted_dates,
#             "ticker": sorted_tickers,
#             "field": sorted_fields,
#         },
#         dims=["datetime", "ticker", "field"],
#     )


def _nested_dict_to_xarray_optimized(data_dict) -> xr.Dataset:
    """
    Convert nested dictionary {ticker: {datetime: {field: value}}} to xarray Dataset.
    Each ticker becomes a data variable in the Dataset.
    """
    # First pass: collect unique values
    dates = set()
    fields = set()
    for ticker_data in data_dict.values():
        for date, field_data in ticker_data.items():
            dates.add(pd.to_datetime(date))
            fields.update(field_data.keys())

    # Convert to sorted lists for indexing
    sorted_dates = sorted(dates)
    sorted_fields = sorted(fields)

    # Create mapping dictionaries for quick index lookups
    date_to_idx = {date: idx for idx, date in enumerate(sorted_dates)}
    field_to_idx = {field: idx for idx, field in enumerate(sorted_fields)}

    # Create a dictionary to store each ticker's data array
    data_vars = {}

    # Process each ticker
    for ticker, ticker_data in data_dict.items():
        # Pre-allocate numpy array for this ticker
        values = np.full((len(dates), len(fields)), np.nan)

        # Fill values using direct numpy indexing
        for date_str, field_data in ticker_data.items():
            date_idx = date_to_idx[pd.to_datetime(date_str)]
            for field, value in field_data.items():
                field_idx = field_to_idx[field]
                values[date_idx, field_idx] = value

        # Add to data_vars dictionary
        data_vars[ticker] = xr.DataArray(
            values,
            dims=['datetime', 'field'],
            coords={
                'datetime': sorted_dates,
                'field': sorted_fields
            }
        )

    # Create Dataset from data_vars
    return xr.Dataset(data_vars)


def getHist(
    securities,
    fields,
    start,
    end=None,
    period="DAILY",
    overrides: Dict = None,
    request_params: Dict = None,
    use_dvd_adj: str = True,
    debug=False,
    to_frame=False,
    index_split: str = " ",
    rsplit: bool = True,
) -> Tuple[xr.Dataset, Dict]:
    """Get historical data with multiple fields.

    Parameters
    ----------
    securities : [type]
        [description]
    fields : [type]
        [description]
    start : [type]
        BBG format "yyyyMMdd""
    end : [type], optional
        [description], by default None
    period : str, optional
        [description], by default "DAILY"
    overrides : [type], optional
        [description], by default None
    debug : bool, optional
        [description], by default False
    to_frame : bool, optional
        Convert from xarry to wide dataframe, by default False
    index_split: bool, optional
        Split index column value by given symbol, default using rsplit

    Returns
    -------
    Tuple[xr.Dataset, Dict]
        if to_frame set to True then return dataframe, otherwise xarray.Dataset.

    Raises
    ------
    AssertionError
        [description]
    """
    if len(securities) < 1:
        print("No security given. Do nothing.")
        return None, None

    if end is None:
        end = dt.date.today().strftime("%Y%m%d")

    session = blp.Session()
    if session.start():
        session.openService(REF_DATA)
    else:
        print("Failed to start Bloomberg Session, return none")
        return

    service = session.getService(REF_DATA)
    request = service.createRequest(HIST_REQUEST)

    for sec in securities:
        request.append("securities", sec)

    for fld in fields:
        request.append("fields", fld)

    request.set("startDate", start)
    request.set("endDate", end)
    if period is not None:
        # by making this optional, this method can be used for all data
        # release frequencies
        request.set(PERIODICITY, period)

    if request_params is not None:
        for k, v in request_params.items():
            request.set(k, v)

    if not use_dvd_adj:
        request.set("adjustmentFollowDPDF", "False")

    if overrides is not None:
        override_element = request.getElement("overrides")
        for f in overrides:
            value = overrides[f]
            override = override_element.appendElement()
            override.setElement("fieldId", f)
            override.setElement("value", value)

    session.sendRequest(request)

    tableData = {}
    while True:
        ev = session.nextEvent()

        if (
            ev.eventType() == blp.Event.RESPONSE
            or ev.eventType() == blp.Event.PARTIAL_RESPONSE
        ):
            for msg in ev:
                # print MSG
                if not msg.hasElement("securityData"):
                    print(msg)
                    continue

                securityData = msg.getElement("securityData")
                if securityData.hasElement("security"):
                    security = securityData.getElement("security").getValue()
                else:
                    print("security field not found, pass")
                    continue

                if not securityData.hasElement("fieldData"):
                    print("no fieldData element found, pass")
                    continue

                tableData[security] = {}
                fieldData = securityData.getElement("fieldData")
                for element in fieldData.values():
                    # find date
                    date = element.getElement("date").getValue()
                    tableData[security][date] = {}
                    # print tableData[security]

                    for f in fields:
                        if element.hasElement(f):
                            fval = element.getElement(f).getValue()
                            tableData[security][date][f] = fval
                        else:
                            tableData[security][date][f] = np.nan
            if ev.eventType() == blp.Event.RESPONSE:
                break
        elif is_service_down(ev):
            session.stop()
            raise AssertionError("Bloomberg Service Down.")
        elif debug:
            # print('Other Event')
            for msg in ev:
                print(msg)

    session.stop()

    # old code using xarray, replaced by optimised version
    # result = {}
    # for key in tableData.keys():
    #     df = pd.DataFrame(tableData[key]).T
    #     if len(df) > 0:
    #         df.index = pd.DatetimeIndex(df.index)
    #     result[key] = df

    # # convert to xarry.Dataset
    # output = dict_to_xarray(result)

    # if to_frame:
    #     v = output.to_dataframe().reset_index()
    #     if index_split is not None:
    #         v.columns = extract_id(v.columns, index_split, rsplit=rsplit)
    #     output = pd.melt(
    #         v,
    #         id_vars=["index", "columns"],
    #         value_vars=v.columns[2:],
    #         var_name="id",
    #         value_name="data",
    #     )


    # tableData is a dict with tickers as keys
    output = _nested_dict_to_xarray_optimized(tableData)
    if to_frame:
        # if output is DataArray, name needs to be given.
        # output = output.to_dataframe(name="value").reset_index()
        output = output.to_dataframe().reset_index()

    return output, tableData


def getHistLast(
    securities,
    start,
    end=None,
    field=S.PX_LAST,
    period="DAILY",
    overrides=None,
    debug=False,
    index_split: str = None,
    column_split: str = None,
    rsplit: bool = True,
    colnames_dict: Dict = None,
) -> pd.DataFrame:
    """Shortcut for getHist(), using PX_LAST as defualt.

    Parameters
    ----------
    securities : [type]
        [description]
    start : [type]
        [description]
    end : [type], optional
        [description], by default None
    field : [type], optional
        [description], by default S.PX_LAST
    period : str, optional
        [description], by default "DAILY"
    overrides : [type], optional
        [description], by default None
    debug : bool, optional
        [description], by default False
    index_split : str, optional
        [description], by default None
    rsplit : bool, optional
        [description], by default True

    Returns
    -------
    pd.DataFrame
        [description]
    """
    res, _ = getHist(
        securities,
        fields=[field],
        start=start,
        end=end,
        period=period,
        overrides=overrides,
        debug=debug,
        # to_frame=True,
    )
    # return res.minor_xs(field)
    # convert from xarray to 2d dataframe
    if isinstance(res, xr.Dataset):
        res = (
            res.sel(field=field).to_dataframe().drop(columns="field")
        )
        # res = pd.pivot_table(
        #     res.reset_index(),
        #     index="datetime",
        #     columns="ticker",
        #     values=field,
        # )
        # # for backward compatiblity, rename index name to index, otherwise
        # # it would be called 'datetime'
        # res.index.name = "index"

        if index_split is not None:
            res.index = extract_id(res.index, index_split, rsplit=rsplit)
        if column_split is not None:
            res.columns = extract_id(res.columns, column_split, rsplit=rsplit)

        if colnames_dict is not None:
            res.rename(columns=colnames_dict, inplace=True)

        return res
    else:
        # TODO depreciate pd.Panel
        return res


def bondRecalFromPx(
    isin,
    price,
    sec_type="CORP",
    yas_yld_type=S.yas_ytw_flag,
    result_fields=["YAS_BOND_YLD", "YAS_ISPREAD_TO_GOVT"],
    curve_id=None,
):
    """
    Calculates bond yield and spreads based on given price.
    """
    return bondRecalc(
        isin,
        value=price,
        fieldId=S.yas_bond_px,
        yas_yld_type=yas_yld_type,
        result_fields=result_fields,
        curve_id=curve_id,
    )


def bondRecalc(
    isin,
    value,
    fieldId,
    sec_type="CORP",
    yas_yld_type=S.yas_ytw_flag,
    result_fields=["YAS_BOND_YLD", "YAS_ISPREAD_TO_GOVT"],
    curve_id=None,
):
    overrides = {fieldId: value, S.yas_yld_flag: yas_yld_type}
    if curve_id is not None:
        overrides[S.yas_curve_id] = curve_id

    res = getRefBulk(
        [isin + " " + sec_type], fields=result_fields, overrides=overrides
    )
    return res


def getRefBulk(
    securities,
    fields,
    overrides=None,
    array_mapper=None,  # this is used for search functions
    debug=False,
    to_frame=True,
    index_split: str = None,
):
    """
    Get reference bulk data, e.g. index members for an equity index.
    Parameters:
    securities: array like security IDs
    fields: array like bloomberg fields
    """
    session = blp.Session()
    if session.start():
        session.openService(REF_DATA)
    else:
        print("Failed to start Bloomberg Session, return none")
        return

    service = session.getService(REF_DATA)
    request = service.createRequest(REF_REQUEST)

    for sec in securities:
        request.append("securities", sec)

    for fld in fields:
        request.append("fields", fld)

    if overrides is not None:
        overrides_element = request.getElement("overrides")
        # add overrides
        for f in overrides:
            value = overrides[f]
            override = overrides_element.appendElement()
            override.setElement("fieldId", f)
            override.setElement("value", value)

    #         if fund_per is not None:
    #             override1 = overrides.appendElement()
    #             override1.setElement('fieldId', 'FUND_PER')
    #             override1.setElement('value', fund_per)
    #         if fund_year is not None:
    #             override2 = overrides.appendElement()
    #             override2.setElement('fieldId', 'EQY_FUND_YEAR')
    #             override2.setElement('value', fund_year)

    session.sendRequest(request)

    data = dict()
    while True:
        ev = session.nextEvent()

        if (
            ev.eventType() == blp.Event.RESPONSE
            or ev.eventType() == blp.Event.PARTIAL_RESPONSE
        ):
            for msg in ev:
                if not msg.hasElement("securityData"):
                    print(msg)
                    continue

                securityDataArr = msg.getElement("securityData")
                if securityDataArr.isArray():
                    for item in securityDataArr.values():
                        if item.hasElement("security"):
                            security = item.getElement("security").getValue()
                        else:
                            print("security field not found = ")
                            print(msg)
                            pass
                        if item.hasElement("fieldData"):
                            fieldData = item.getElement("fieldData")
                        else:
                            print("fieldData field not found = ")
                            print(msg)
                            pass

                        # now find the dict that holds data for this security
                        if security in data:
                            secFields = data[security]
                        else:
                            secFields = dict()
                            data[security] = secFields

                        for elm in fieldData.elements():
                            if elm.isArray():
                                if array_mapper is not None:
                                    dd = array_mapper(elm)
                                    secFields[elm.name().__str__()] = dd

                                    if debug:
                                        print("Applyed array_mapper...")
                                        print(elm.name().__str__())
                                        print(dd)
                                else:
                                    # this is the case for bulk data
                                    # find the dict that stores data for this
                                    # field
                                    if elm.name().__str__() in secFields:
                                        dd = secFields[elm.name().__str__()]
                                    else:
                                        dd = dict()
                                        secFields[elm.name().__str__()] = dd

                                    for m in elm.values():
                                        # there may be multiple data items
                                        # returned in bulk data for a single
                                        # bloomberg data field,
                                        # go through them all and store
                                        # item as a list
                                        # Using list ensures the order matches
                                        # what is being returned in the
                                        # bloomberg message.
                                        for v in m.elements():
                                            key = v.name().__str__()
                                            val = v.getValue()

                                            if isinstance(val, bytes):
                                                val = val.decode(
                                                    "utf-8"
                                                ).strip()
                                            elif isinstance(val, str):
                                                # try:
                                                #     val = val.decode(
                                                #         "utf-8"
                                                #     ).strip()
                                                # except AttributeError:
                                                #     # windows py3.6 no decode?
                                                #     val = val.strip()
                                                val = val.strip()

                                            if key in dd:
                                                dd[key].append(val)
                                            else:
                                                # s = set()
                                                # s.add(val)
                                                # no assumption made about
                                                # result
                                                s = []
                                                s.append(val)
                                                dd[key] = s
                            else:
                                # no array data, store and go.
                                secFields[elm.name().__str__()] = elm.getValue()
                else:
                    # no security data array - that might not be possible.
                    print(
                        "Possible bug/data error, "
                        + "expecting SecurityDataAarry = "
                    )
                    print(msg)

            if ev.eventType() == blp.Event.RESPONSE:
                break
        elif is_service_down(ev):
            raise AssertionError("Bloomberg Service Down.")
        elif debug:
            # not a response, let's print it for now
            for m in ev:
                print(m)
    if to_frame:
        data = pd.DataFrame.from_dict(data, orient="index")
        if index_split is not None:
            data.index = extract_id(data.index, split_sym=index_split)

    return data


def ipo_data(tickers, bb_start, bb_end):
    """
    Return a list of recent IPOs after the start date with some historical data
    """
    ipo_dates = getRefBulk(tickers, ["EQY_INIT_PO_DT"])
    ipo = pd.DataFrame.from_dict(ipo_dates, orient="index")
    ipo = ipo.dropna()
    start_dt = dt.datetime.strptime(bb_start, "%Y%m%d")
    new = ipo[ipo.EQY_INIT_PO_DT > start_dt.date()]
    new_ipo, _ = getHist(
        new.index,
        ["PE_RATIO", "EARN_YLD", "PX_TO_BOOK_RATIO"],
        bb_start,
        bb_end,
    )
    ipo_pd = pd.Panel.from_dict(new_ipo)
    return ipo_pd


def genDates(days):
    end = dt.date.today()
    delta = dt.timedelta(days=days)
    start = end + delta
    if days < 0:
        return start, end
    else:
        return end, start


def bbDate(arg):
    """
    Formats a datetime date to Bloomberg date format.
    """
    return arg.strftime(BBG_DATE_FORMAT)


# def indexMembers(index):
#     return getRefBulk([index], fields=[INDX_MEMBERS])


def get_sector_info(tickers):
    return getRefBulk(tickers, S.GICS_SECTOR_FIELDS, to_frame=True)


def get_eq_index_members(index_code: str, sector_data=False):
    # TODO add feature to get GICS sector data.
    members = getRefBulk([index_code], [S.INDX_MEMBERS])
    if index_code not in members.index:
        raise ValueError("Cannot find execpted data.")
    value_dict = members.loc[index_code, S.INDX_MEMBERS]
    ind_memb = value_dict.get("Member Ticker and Exchange Code")

    if sector_data:
        ind_tickers = [x + " Equity" for x in ind_memb]
        sector_frame = get_sector_info(ind_tickers)
        return sector_frame
    else:
        return ind_memb


def get_future_codes(fut: str, month: str, year, fut_type="Comdty") -> str:
    # code = fut + month + str(year)[-1:]
    # if int(year) == dt.datetime.today().year:
    #     code += ' ' + fut_type
    # else:
    #     code += ' ' + str(year)[-2:] + ' ' + fut_type

    code = fut + month
    if int(year) == dt.datetime.today().year:
        code += str(year)[-1] + " " + fut_type
    else:
        code += str(year)[-2:] + " " + fut_type

    return code


def get_future_codes_multi(
    fut: str, years: list, month_codes=S.FUT_MONTH_CODE, fut_type="Comdty"
):
    if sys.version_info.major > 2:
        zip_func = zip
    else:
        zip_func = itr.izip

    tickers = [
        get_future_codes(fut, month, year, fut_type=fut_type)
        for month, year in zip_func(
            itr.cycle(month_codes), np.sort(years * len(month_codes))
        )
    ]
    return np.unique(tickers)


def get_first_notice(futures):
    dates = getRefBulk(futures, fields=[S.FUT_NOTICE_FIRST], to_frame=True)
    # df = pd.DataFrame.from_dict(dates, orient='index')
    dates.sort_values(by=S.FUT_NOTICE_FIRST, inplace=True)
    return dates


def roll_futures(
    px_df,
    notice_dates,
    roll_period=5,
    notice_column="FUT_NOTICE_FIRST",
    verbose=False,
):
    """
    Assumption: px_df has prices for all valid dates (good biz dates).

    Returns:
    A tuple of :
    weights : contract weights on roll days
    px : roll-adjusted price series
    """

    if roll_period < 1:
        print("Roll period must be greater than 0. Reset to default 5.")
        roll_period = 5

    roll_up_weights = (
        np.linspace(start=1, stop=roll_period, num=roll_period) / roll_period
    )
    roll_down_weights = 1.0 - roll_up_weights

    weights = pd.DataFrame(
        np.zeros((len(px_df), len(px_df.columns))),
        index=px_df.index,
        columns=px_df.columns,
    )

    # work out adjustments in time order for each future
    notice_dates.sort_values(by=notice_column, inplace=True)
    next_start_dates = None
    day_delta = pd.Timedelta(1, "D")
    for contract, first_notice in notice_dates.itertuples():
        if contract not in px_df.columns:
            continue

        before_notice = first_notice - day_delta

        # find start of current contract
        if next_start_dates is not None and len(next_start_dates) > 0:
            start_date = next_start_dates.sort_values()[0] - day_delta
        else:
            start_date = px_df[contract].first_valid_index()

        # determine roll down and up dates
        # using 2x roll days just to be safe.
        roll_down_dates = pd.date_range(
            end=before_notice, freq="B", periods=2 * roll_period
        )
        # find the roll down dates that exist in contract price series
        # this essentially deals with any non-trading days.
        mask = roll_down_dates.isin(weights.loc[:, contract].index)
        # find the last
        roll_down_dates = roll_down_dates[mask][-roll_period:]

        # adjust roll up dates if needed. This is only needed if the contract
        # is being rolled into.
        if next_start_dates is not None and len(next_start_dates) > 0:
            next_start = next_start_dates[0] + day_delta
            weights.loc[next_start:before_notice, contract] = 1
            weights.loc[next_start_dates, contract] = roll_up_weights
        elif next_start_dates is None:
            weights.loc[start_date:before_notice, contract] = 1

        # adjust roll down period iff we are in the roll window
        weights.loc[roll_down_dates, contract] = roll_down_weights[
            : len(roll_down_dates)
        ]
        # update next start dates for next contract, used for rolling it up.
        next_start_dates = roll_down_dates
        if verbose:
            print(
                (
                    "Contract = {}, Before Notice = {:%Y-%m-%d}, "
                    + "Start Date = {:%Y-%m-%d}, Roll_dates={}."
                ).format(contract, before_notice, start_date, roll_down_dates)
            )

    # sanity check, weights must sum to one at all times
    check_res = np.isclose(weights.sum(axis=1), 1.0)
    if not check_res.all():
        pp = pprint.PrettyPrinter(indent=2)
        pp.pprint(weights.loc[~check_res].index)
        raise ValueError("Something is wrong, max weight != 1. Shown below.")
    elif verbose:
        print("Weights sanity check PASSED.")

    adj_px = (weights * px_df).sum(axis=1)
    # adjust for holidays. Based on bloomberg data I have seen prices printed
    # on public holidays. So we can try to rmoeve it by doing the following
    adj_px.replace(0, np.nan, inplace=True)
    adj_px.dropna(inplace=True)

    return (weights, adj_px)


def roll_future_rtns(px, wgts):
    """
    Roll futures and calculate daily returns and return adjusted price series.
    This should match Bloomberg's ratio adjusted price series.

    However, with Bloomberg's N:05_0_R adjustment. The days 05 parameter is
    based on calendar days, whereas my method is based on business days.

    Parameters:
    px:
        Individual contract prices
    wgts:
        weights for individual contracts, every row should should sum to 1

    Returns:
        Adjusted daily return and price series, joined with input contract
        prices.
    """
    # check all weights are valid
    # every row should sum to 1
    total_w = wgts.sum(axis=1)
    if not np.allclose(total_w, 1.0):
        raise AssertionError(
            "Some weights don't sum to 1. "
            "Try moving start date back further."
        )

    # find individial contract returns.
    # work out portfolio return based on weights. return on T is the average
    # returns of the active contract(s) weighted by T-1's weights.
    rtns = px.pct_change()
    adj_rtns = (rtns * wgts.shift(1)).sum(axis=1)

    # find current future, work out latest price
    curr_fut = wgts.idxmax(axis=1).tail(1)
    latest_px = px[curr_fut].values[-1][0]

    # work out adjusted price
    # 1. work out daily adjustment factor, T-1's price is T's price divided
    # by factor of (1 + return from T-1 to T+1)
    # epsilon is added for avoid log(0) problem in extreme cases, i.e return
    # -100%...
    epsilon = 1e-100
    cum_rtn = np.exp(np.log(1 + adj_rtns[::-1] + epsilon).cumsum())
    adj_px = latest_px / cum_rtn[::-1]

    # shift back by 1 to match dates because adjustment factor for T-1
    # is only known on T, when the return is known.
    adj_px = adj_px.shift(-1)
    # update last px
    adj_px.iloc[-1] = latest_px

    df = px.assign(adj_px=adj_px, adj_rtns=adj_rtns)
    return df


def get_futures_px(
    fut: str,
    bb_start: str,
    bb_end: str = None,
    field="PX_LAST",
    month_codes=S.FUT_MONTH_CODE,
    fut_type="Comdty",
    roll_period=5,
    verbose=False,
) -> dict:
    """
    Get continuous futures prices.

    Example:

    px = blp.get_futures_px('TY', bb_start='20090816', verbose=True)
    px.get('prices').adj_px.plot()
    # pct returns are stored in
    px.get('prices').adj_rtns

    Parameters:
    fut:    Bloomberg future code, e.g. TY, RX, etc.
    bb_start:   Start date, Bloomberg API format, e.g. 20100101
    bb_end:     End date, Bloomberg API format. default None, in which case
                today's date is used.
    field:      Bloomberg price field, default PX_LAST
    month_codes: Bloomberg future month code, default [H, M, U, Z]
    fut_type:   Bloomberg future key, {'Comdty', 'Index'}. Default 'Comdty'.
    roll_period: Number of days used to roll. Default 5.

    Retruns:
    Dictionary with results. Keys include:
    weights: roll weights
    prices: Future prices, included 'adj_px' column which is the continuous
            contract price),
    first_notice: first notice dates
    future_codes: Bloomberg future codes.
    """
    # 0. work out implied years
    start_year = int(bb_start[:4])
    if bb_end is None:
        bb_end = dt.date.today().strftime("%Y%m%d")
    end_year = int(bb_end[:4])
    years = [x for x in range(start_year, end_year + 1, 1)]

    # 1. get futures code
    fut_codes = get_future_codes_multi(
        fut, years, month_codes=month_codes, fut_type=fut_type
    )
    if verbose:
        print(fut_codes)
    # 2. get notice dates
    notice_dates = get_first_notice(fut_codes)
    if verbose:
        print("Notice dates: {}".format(len(notice_dates)))
    # 3. get price data
    px_df = getHistLast(fut_codes, start=bb_start, end=bb_end, field=field)
    if verbose:
        print(f"Data length: {len(px_df)}, columns {len(px_df.columns)}")
    # 4. clean data
    px_df.replace(0, np.nan, inplace=True)
    px_df.dropna(axis=1, how="all", inplace=True)
    # 5. roll futures
    w, px_adj = roll_futures(
        px_df, notice_dates, roll_period=roll_period, verbose=verbose
    )

    # px_df['adj_px'] = px_adj
    # work out daily return and return adjusted px
    adj_df = roll_future_rtns(px_df, w)

    res = dict()
    res["prices"] = adj_df
    res["weights"] = w
    res["avg_px"] = px_adj
    res["first_notice"] = notice_dates
    res["future_codes"] = fut_codes

    return res


def get_futures_bb(
    futs, start_bb, end_bb=None, method="N:05_0_R", field=S.PX_LAST
):
    """
    Get futures data from Bloomberg, using Bloomberg's roll
    calculations.

    Args:
        futs (TYPE): Description
        start (TYPE): Description
        end (None, optional): Description
        method (str, optional): Description
        field (TYPE, optional): Description

    Returns:
        TYPE: Description
    """
    futs_new = [
        "{} {} {}".format(f.split(" ")[0], method, f.split(" ")[1])
        for f in futs
    ]

    data = getHistLast(futs_new, start=start_bb, end=end_bb, field=field)
    return data


def index_fundamentals(
    index,
    start=None,
    end="-0FQ",
    period=S.QUARTERLY,
    fund_per=None,
    fund_year=None,
):
    members = get_eq_index_members(index)
    rt_frame = getRefBulk(members, S.EQ_RT_FIELDS, to_frame=True)

    if start is not None:
        fund_data = getHist(
            members,
            fields=S.EQ_RPT_NONFIN_FIELDS,
            start=start,
            end=end,
            period=period,
        )
        fund_panel = pd.Panel.from_dict(fund_data[0])
        return (members, rt_frame, fund_panel)
    else:
        return (members, rt_frame)


# def store_by_field(file_store,
#                    data,
#                    axis=0,
#                    mode='a',
#                    verbose=True):
#     '''
#     Store data by data field in HDFS format, e.g. PX_ASK, PX_BID.

#     Parameters:
#     file_store:
#         data file location
#     data:
#         pandas panel data
#     axis:
#         default 0. 0 - minor_axis, 1: items_axis, 2: major axis.
#     '''
#     if axis == 0:
#         for k in data.minor_axis:
#             _store_key(data.loc[:, :, k],
#                        file_store=file_store, key=k, mode=mode)
#     elif axis == 1:
#         for k in data.item_axis:
#             _store_key(data.loc[k, :, :],
#                        file_store=file_store, key=k, mode=mode)
#     elif axis == 2:
#         for k in data.major_axis:
#             _store_key(data.loc[:, k, :],
#                        file_store=file_store, key=k, mode=mode)
#     else:
#         raise AssertionError('axis must be {0, 1, 2}.')


def _store_key(df, file_store, key, mode, verbose):
    """
    Stores a dataframe in a HDF5 file store with given key and mode.
    """
    if verbose:
        print("Store key: {}...".format(key), end="")
    df.to_hdf(file_store, key=key, mode=mode, complevel=9)
    if verbose:
        print("  Done.")


# def to_xarray(dict_frames):
#     dvars = {k: xr.DataArray(v, dims=['dates', 'columns'])
#              for k, v in dict_frames.items()}
#     ds = xr.Dataset(dvars)

#     return ds


def get_baml_index_hist(ind, start_iso, fields, end_iso=None, freq="B"):
    if end_iso is None:
        end_iso = dt.datetime.today().strftime("%Y-%m-%d")

    dates = pd.date_range(start_iso, end_iso, freq=freq)
    # non_empty = []

    data = list()
    for i in tqdm.tqdm(range(len(dates))):
        d = dates[i]
        overrides = {S.MLI_DATE: d.strftime(BBG_DATE_FORMAT)}
        x = getRefBulk(ind, fields=fields, overrides=overrides)
        if len(x) > 0:
            # non_empty.append(d)
            xt = x.T
            xt.index = [d]
            data.append(xt)

    df = pd.concat(data)
    # df.index = dates

    return df


def get_baml_index_mv(ind, start_iso, end_iso=None, freq="B"):
    fields = [S.MLI_FUMLI_FULL_MKT_VAL]
    return get_baml_index_hist(
        ind, start_iso, fields=fields, end_iso=end_iso, freq=freq
    )


def get_corp_fundamental(
    tickers,
    fields,
    start_bb,
    end_bb=None,
    period=S.QUARTERLY,
    adjusted=True,
    resample=True,
    debug=False,
):
    """
    Get corporate fundamental data based on report frequency and adjusted flag.

    Parameters
    ----------
    tickers : TYPE

    fields : TYPE

    start_bb : TYPE

    end_bb : None, optional

    period : TYPE, optional
        Default 'QUARTERLY'.
    adjusted : bool, optional
        Default True
    debug : bool, optional
        Default False

    Returns
    -------
    xarray.Dataset, resampled at either business quarter or year end.
    """
    if adjusted:
        overrides = {S.FA_ADJUSTED: S.FA_ADJUSTED_ADJ}
    else:
        overrides = None

    data, raw_data = getHist(
        tickers,
        fields,
        start_bb,
        end_bb,
        period=period,
        overrides=overrides,
        to_frame=False,
        debug=debug,
    )

    if debug:
        return raw_data

    if resample:
        if period == S.YEARLY:
            if pd.__version__ < '2.0.0':
                freq = 'A'
            else:
                freq = "YE"
        # don't think xarray supports semi annual frequency
        # elif period == S.SEMI_ANNUALLY:
        #     freq = 'S'
        else:
            if pd.__version__ < '2.0.0':
                freq = 'Q'
            else:
                freq = "QE"
        data = data.resample({"datetime": freq}).last()

    return data


def get_interval_sums(
    tickers: Iterable[str], field: str, adjusted: bool = False
) -> pd.DataFrame:
    """Use bloomberg's INTERVAL_SUM feature to rolling fundamental data.

    Parameters
    ----------
    tickers : Iterable[str]
        [description]
    field : str
        fundamental field to sum
    adjusted : bool, optional
        use adjusted financials if True, by default False

    Returns
    -------
    pd.DataFrame
        [description]
    """
    overrides = {
        # e.g. field = 'IS_COG_AND_SERVICES_SOLD'
        "MARKET_DATA_OVERRIDE": field,
        "CALC_INTERVAL": "12m",
    }

    if adjusted:
        overrides[S.FA_ADJUSTED] = S.FA_ADJUSTED_ADJ

    df = getRefBulk(
        tickers, fields=[INTERVAL_SUM], overrides=overrides, to_frame=True
    )
    df.rename(columns={INTERVAL_SUM: field}, inplace=True)

    return df


def get_corp_report_period(tickers: Iterable[str]):
    """
    Get corporate reference data:
    * primary report periodicity,
    * adjusted financials indicator,
    * BICS level 1 sector,
    * BICS level 2 industry

    Parameters
    ----------
    tickers : list of strings
        Bloomberg equity tickers

    Returns
    -------
    DataFrame
    """
    df = getRefBulk(
        tickers,
        fields=[
            S.PRIMARY_PERIODICITY,
            S.ADJUSTED_FINANCIALS_INDICATOR,
            S.BICS_LEVEL_1_SECTOR_NAME,
            S.BICS_LEVEL_2_INDUSTRY_GROUP_NAME,
        ],
        to_frame=True,
    )
    return df


def split_report_group(df, sector_key=None, debug=False):
    """
    Split tickers into (primary_periodicity, adjusted) groups.

    Parameters
    ----------
    df : TYPE

    debug : bool, optional


    Returns
    -------
    dict
    """
    assert S.PRIMARY_PERIODICITY in df.columns
    assert S.ADJUSTED_FINANCIALS_INDICATOR in df.columns

    if sector_key is not None:
        group_key = [
            sector_key,
            S.PRIMARY_PERIODICITY,
            S.ADJUSTED_FINANCIALS_INDICATOR,
        ]
    else:
        group_key = [S.PRIMARY_PERIODICITY, S.ADJUSTED_FINANCIALS_INDICATOR]

    grp = df.groupby(group_key)
    d = dict()
    for name, values in grp:
        if debug:
            print("%s -> item count: %d" % (name, len(values.index)))
        d[name] = values.index
    return d


def _convert_periodicity(period_str):
    if period_str.startswith("Semi-Annual"):
        return S.SEMI_ANNUALLY
    elif period_str.startswith("Quarterly"):
        return S.QUARTERLY
    else:
        return S.YEARLY


def get_corp_data(
    tickers: Iterable[str],
    fields_dict,
    start_bb,
    end_bb=None,
    fields_default=S.EQ_RPT_NONFIN_FIELDS,
    use_adjusted=True,
    debug=False,
):
    """


    Parameters
    ----------
    tickers : TYPE

    fields_dict : dict
        dict of sector field mappings. This allows different fundamental fields
        to be used for different sectors. If a sector is not found in this
        dict, the default `fields_default` fields are used.

        Currently, BICS level 2 industry names are used, which allows us to
        separate banks, insurance and REITS.
    start_bb : TYPE

    end_bb : None, optional

    fields_default : TYPE, optional

    use_adjusted : bool, optional

    debug : bool, optional


    Returns
    -------
    (data, reference)
    data : dict of xarray.DataArray
        fundamental data metrics
    reference : DataFrame
        reference data for tickers, returned by get_corp_report_period()
    """
    # get reference data
    if debug:
        print("Getting corp reference data.")
    corp_ref = get_corp_report_period(tickers)

    # TODO: determine which fields to use based on sector info

    # split by report frequency and adjusted indicator
    if debug:
        print("Split into query groups...")
    # the sector split is there so that we can use different BBG fields for
    # different sectors, if needed.
    ref_dict = split_report_group(
        corp_ref, sector_key=S.BICS_LEVEL_2_INDUSTRY_GROUP_NAME, debug=debug
    )

    # get data for each group
    data = dict()
    for grp_key, values in ref_dict.items():
        sector, periodicity, adjusted = grp_key
        # if adjusted date is available, use the provided use_adjusted flag.
        adjusted = (adjusted == "Y") | use_adjusted

        fields = fields_dict.get(sector, None)
        if fields is None:
            print(
                f"Sector {sector} field mapping cannot be found. Use defaults."
            )
            fields = fields_default

        if debug:
            print(
                "Getting data for %s, adjusted = %s, count = %d"
                % (periodicity, adjusted, len(values))
            )

            print(f"Data fields: {fields}")

        # returns dataset
        period = _convert_periodicity(periodicity)
        ds = get_corp_fundamental(
            values,
            fields,
            start_bb,
            end_bb=end_bb,
            period=period,
            adjusted=adjusted,
            debug=debug,
        )
        # df[PRIMARY_PERIODICITY] = periodicity
        # df['is_adjusted'] = adjusted
        # append_existing(data, sector, df)
        _append_existing(data, sector, ds)

    # combine data
    results = dict()
    for k, vs in data.items():
        if debug:
            print("Concatenating %s datasets, size: %d" % (sector, len(vs)))
        # vs is a list
        results[k] = xr.merge(vs)

    return results, corp_ref


def _append_existing(data, key, df):
    """
    Append or add to a dict of lists.

    Parameters
    ----------
    data : dict
        dict of lists
    key : TYPE
        key for data dict
    df : TYPE
        item to append or add to list

    Returns
    -------
    original data dict.
    """
    ld = data.get(key, None)
    if ld is None:
        data[key] = [df]
    else:
        ld.append(df)
        data[key] = ld
    return data


def add_type(x, sec_type: str, source: str = None):
    if source is None:
        return [f"{v} {sec_type}" for v in x]
    else:
        return [f"{v}@{source} {sec_type}" for v in x]


def extract_id(
    ids, split_sym: str = None, fixed_len: int = None, rsplit: bool = True
):
    if split_sym is not None:
        if rsplit:
            return [x.rsplit(split_sym, 1)[0] for x in ids]
        else:
            return [x.split(split_sym, 1)[0] for x in ids]
    elif fixed_len > 0:
        return [x[:fixed_len] for x in ids]
    else:
        raise ValueError("Either split_sym or fixed_len must be given.")


def _parse_array_element(field_name: str, element):
    if element.hasElement(field_name):
        field = element.getElement(field_name)
        try:
            value = field.getValue()
            return value
        except blp.IndexOutOfRangeException:
            # error / missing data
            return np.nan
    else:
        print(element)
        raise ValueError(f"Unusual format: No {field_name}.")


def auction_mapper(array_element) -> Dict:
    """Map auction bulk data to dict, example:

    SOVEREIGN_BD_REOPENING_AUCT_DATA[] = {
        SOVEREIGN_BD_REOPENING_AUCT_DATA = {
            Auction Date = 2019-10-18
            Announcement Date = 2019-10-12
            Settlement Date = 2019-10-21
            Increase Amount = "Y"
            Auction Flag = 34600000.000000
            Increase Price = 102.030000
            Increase Yield = 3.800000
        }
        SOVEREIGN_BD_REOPENING_AUCT_DATA = {
            Auction Date = 2019-08-30
            Announcement Date = 2019-08-23
            Settlement Date = 2019-09-02
            Increase Amount = "Y"
            Auction Flag = 37850000.000000
            Increase Price = 103.570000
            Increase Yield = 3.690000
        }
        SOVEREIGN_BD_REOPENING_AUCT_DATA = {
            Auction Date = 2019-08-16
            Announcement Date = 2019-08-09
            Settlement Date = 2019-08-19
            Increase Amount = "Y"
            Auction Flag = 37560000.000000
            Increase Price = 104.550000
            Increase Yield = 3.630000
        }
    }

    Parameters
    ----------
    array_element : [type]
        [description]
    """
    # The columns consist of Auction Date, Announcement Date, Settlement Date,
    # Auction flag, Increase Amount, Increase Price and Increase Yield
    # print(array_element)

    auction_date = "Auction Date"
    announce_date = "Announcement Date"
    sett_dt = "Settlement Date"
    # this is a bloomberg bug, auc flag and inc amount is reversed..
    inc_amt = "Auction Flag"
    auc_flag = "Increase Amount"
    inc_px = "Increase Price"
    inc_yld = "Increase Yield"

    field_list = [
        auction_date,
        announce_date,
        sett_dt,
        auc_flag,
        inc_amt,
        inc_px,
        inc_yld,
    ]

    # dict of lists
    auction_data = dict()

    for element in array_element.values():
        for name in field_list:
            try:
                value = _parse_array_element(name, element)
            except ValueError:
                value = np.nan
            existing = auction_data.get(name, [])
            existing.append(value)
            auction_data[name] = existing
    return auction_data


def get_auction_dates(bonds, debug=False) -> pd.DataFrame:
    """Convenient method for getting auction dates.

    Parameters
    ----------
    bonds : [type]
        List of bond identifiers
    debug : bool, optional
        debug flag, by default False

    Returns
    -------
    pd.DataFrame
        [description]
    """
    d = getRefBulk(
        bonds,
        fields=[S.SOVEREIGN_BD_REOPENING_AUCT_DATA],
        array_mapper=auction_mapper,
        debug=debug,
        to_frame=True,
    )

    df_list = []
    for ind, s in d.iterrows():
        #     print(ind)
        df = pd.DataFrame(s.values[0])
        key = ind.split(" ")[0]
        df = df.assign(key=key)
        df_list.append(df)
    df = pd.concat(df_list, axis=0)

    return df.reset_index(drop=True)


def interval_sum(
    tickers: Iterable[str], field: str, interval: str = "12m", **kwargs
):
    """For an fundamental reporting field, this method will get trailing
    12M numbers, based on the parameters provided.

    Check FPDP<GO> setting for Non-GAAP Adjustment, setting this to Adjusted
    will always get ADJUSTED numbers.

    Parameters
    ----------
    tickers : Iterable[str]
        [description]
    field : str
        field of interest, e.g. COGS, EBITDA
    interval : str, optional
        [description], by default "12m"

    Returns
    -------
    [type]
        [description]
    """
    overrides = {"MARKET_DATA_OVERRIDE": field, "CALC_INTERVAL": interval}
    df = getRefBulk(
        tickers, fields=["INTERVAL_SUM"], overrides=overrides, **kwargs
    )
    return df



def get_fed_funds() -> pd.DataFrame:
    data = getHistLast(["FDTR Index"], start="19500101", column_split=" ")
    return data


if __name__ == "__main__":
    import pprint as pp

    # import sys

    print(sys.version_info)

    print("Start blptools.py testing...")
    futs = get_future_codes_multi("TY", ["2014", "2015", "2016"])
    pp.pprint(futs)
    if len(futs) == 12:
        print("Future codes: Passed.")
    else:
        print("Future codes: FAILED.")
        raise AssertionError("Failed to generate correct future codes.")

    print("Testing SRCH: USD_KFW...")
    data = get_srch_bonds(srch_name="USD_KFW")
    if len(data) < 1:
        raise AssertionError("Failed to get SRCH results for USD_KFW.")
    else:
        print("SRCH data size: {}, Passed.".format(len(data)))

    print("Index Member test... DAX")
    data = get_eq_index_members("DAX Index", sector_data=True)
    if len(data) > 25:
        print("Index Member for Dax: {:d}, Passed.".format(len(data)))
    else:
        raise AssertionError("Index Member function failed.")

    print("Test(s) completed.")
