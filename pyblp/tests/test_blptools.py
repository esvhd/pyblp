import pandas as pd
import numpy as np
import xarray as xr

# from pyblp.symbols import *
# import pyblp.symbols as S


def _generate_corp_dataset(
    tickers=["nvda", "intc", "amzn"],
    columns=["EARN_YLD", "EBITDA_MARGIN", "TOTAL_DEBT"],
):
    index = pd.DatetimeIndex(["2017-09-30", "2017-12-31", "2018-03-31"])

    dim_x = len(index)
    dim_y = len(columns)

    ds = xr.Dataset(
        {
            x: (["index", "columns"], np.random.randn(dim_x, dim_y))
            for x in tickers
        },
        coords={"columns": columns, "index": index},
    )
    return ds


def test_xr_merge_same_columns():
    d1 = _generate_corp_dataset()
    d2 = _generate_corp_dataset(["ms", "bac", "jpm"])

    m = xr.merge([d1, d2])

    assert len(d1.columns) == len(m.columns), "Columns changed."

    assert len(m.data_vars) == (len(d1.data_vars) + len(d2.data_vars))


def test_xr_merge_diff_columns():
    d1 = _generate_corp_dataset()
    d3 = _generate_corp_dataset(["ms", "bac"], columns=["net_income", "roe"])

    m = xr.merge([d1, d3])

    assert len(m.columns) == (
        len(d1.columns) + len(d3.columns)
    ), "Merged columns different to expected size."

    assert len(m.data_vars) == (
        len(d1.data_vars) + len(d3.data_vars)
    ), "Merged data vars different to expected size."

    # 1 duplicated column
    d5 = _generate_corp_dataset(
        ["ms", "bac"], columns=["net_income", "roe", "EARN_YLD"]
    )
    m = xr.merge([d1, d5])

    assert (
        len(m.columns) == (len(d1.columns) + len(d5.columns)) - 1
    ), "Merged columns different to expected size."

    assert len(m.data_vars) == (
        len(d1.data_vars) + len(d5.data_vars)
    ), "Merged data vars different to expected size."


# if __name__ == '__main__':
#     xr_merge_diff_columns()
#     xr_merge_same_columns()
