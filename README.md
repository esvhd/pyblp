# Bloomberg API Python Tools

Python 3.x only


## Dependencies

* Bloomberg C/C++ and Python API
* `xarray`
* `tqdm`

To install external dependenceis:

```
conda install -c defaults tqdm
conda install -c conda-forge xarray
```
